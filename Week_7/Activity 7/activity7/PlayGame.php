<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-17
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Activity 6.2
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 6.2
 * 2. Object Oriented Programming - SuperHero
 * 3. Game to watch super heroes battle
 * ---------------------------------------------------------------
 */

require_once 'Batman.php';
require_once 'Superman.php';

$batman = new Batman();
$superman = new Superman();

echo "Let the super hero battle begin.<br />";
while (!$batman->isDead() && !$superman->isDead())
{
    $superman->Attack($batman);
    if ($batman->isDead()) 
    {
        echo $superman->getName() . " has won the game.<br />";
        break;
    }
    
    $batman->Attack($superman);
    if ($superman->isDead())
    {
        echo $batman->getName() . " has won the game.<br />";
        break;
    }
}

echo "The intense battle is over.<br />";