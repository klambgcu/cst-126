<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-04-26
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Activity 4
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 4
 * 2. Obtain form data
 * 3. Handle Login Validation
 * 4. Transistion from procedural connection to PDO object
 * 5. Added Reusable code for retrieving and display all users
 * ---------------------------------------------------------------
 */

require_once('utility.php');

 ?>

<!DOCTYPE html>
<html>
	<!--  the head section -->
	<head>
		<title>Login Response</title>
	</head>

	<!-- the body section -->
	<body>
	<main>
		<h1>Login Response</h1>
		<h2>Login was successful: <?php echo " " . $username ?></h2>
		<br />
		<a href="whoAmI.php">Who Am I</a>
		<br />
		<br />
		
<?php

    // Generate an array of users
    /*
     **************************************
     * Execute this only once for the activity 7 requirement 
    $user_list = array (
        array("firstname1", "lastname1", "username1", "password1"),
        array("firstname2", "lastname2", "username2", "password2"),
        array("firstname3", "lastname2", "username3", "password3"),
        array("firstname4", "lastname2", "username4", "password4"),
        array("firstname5", "lastname2", "username5", "password5"),
    );

    // Insert the list
    insertUsers($user_list);
     **************************************
     */

 	$users = getAllUsers();
    include('_displayUsers.php'); 
 ?>

	</main>
	</body>

</html>