<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-24
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Activity 7
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 7
 * 2. Reusable functions
 * 3. Introduce InsertUsers functions
 * ---------------------------------------------------------------
 */

require_once('myfuncs.php');

/*
 * Function to insert an array of users
 * Param: user_array: array containing user definitions
 * 0 = firstname, 1= lastname, 2=username, 3=password
 */
function insertUsers($users_array)
{
    try
    {
        // Get Database Connection
        $db = dbConnect();
        
        for ($i = 0; $i < count($users_array); $i++)
        {
            // Define SQL prepare statement and bind values
            $sql = "INSERT INTO users (FIRST_NAME, LAST_NAME, USERNAME, PASSWORD) " .
                   "VALUES (:firstname, :lastname, :username, :password)";

            $statement1 = $db->prepare($sql);
            $statement1->bindValue(':firstname', $users_array[$i][0]);
            $statement1->bindValue(':lastname',  $users_array[$i][1]);
            $statement1->bindValue(':username',  $users_array[$i][2]);
            $statement1->bindValue(':password',  $users_array[$i][3]);

            // Execute insert query
            $statement1->execute();
        }
    
    } catch(PDOException $e)
    {
        $error_message = $e->getMessage();
        include('database_error.php');
        exit();
    }
    
    // Close statement and connection
    $statement1->closeCursor();
    $statement1 = null;
    $db = null;

}



function getAllUsers()
{
    // Get Database Connection
    $db = dbConnect();
    
    // Define SQL prepare statement and bind values
    $sql = "SELECT ID, FIRST_NAME, LAST_NAME FROM users";
    $statement1 = $db->prepare($sql);
    
    // Execute select query
    $statement1->execute();   
    
    // return records as associative array - could use fetchAll
    $statement1->setFetchMode(PDO::FETCH_ASSOC);
    
    // add records to users array
    $users = array();
    $index = 0;
    while ($row = $statement1->fetch())
    {
        $users[$index] = array($row["ID"], $row["FIRST_NAME"], $row["LAST_NAME"]);
        ++$index;
    }
    
    // Close statement and connection
    $statement1->closeCursor();
    $statement1 = null;
    $db = null;
    
    return $users;
}

function getUsersByFirstName($searchPattern)
{   
    // Get Database Connection
    $db = dbConnect();
    
    // Define SQL prepare statement and bind values
    $sql = "SELECT ID, FIRST_NAME, LAST_NAME FROM users WHERE FIRST_NAME LIKE :searchPattern";
    $statement1 = $db->prepare($sql);
    $statement1->bindValue(':searchPattern', $searchPattern);

    // Execute select query
    $statement1->execute();

    // return records as associative array - could use fetchAll
    $statement1->setFetchMode(PDO::FETCH_ASSOC);
    
    // add records to users array
    $users = array();
    $index = 0;
    while ($row = $statement1->fetch())
    {
        $users[$index] = array($row["ID"], $row["FIRST_NAME"], $row["LAST_NAME"]);
        ++$index;
    }
    
    // Close statement and connection
    $statement1->closeCursor();
    $statement1 = null;
    $db = null;
 
   // Return null if zero records found
    if (count($users) == 0)
        return null;
    else
        return $users;
}

?>
