<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-17
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Activity 6.2
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 6.2
 * 2. Object Oriented Programming - SuperHero
 * 3. One of the super heroes
 * ---------------------------------------------------------------
 */

require_once 'SuperHero.php';

class Superman extends SuperHero
{
    function __construct()
    {
        // Call Base Constructor with name and random health 1-1000.
        parent::__construct("Superman", rand(1,1000));
    }
}

