<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-04-26
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone (Blog Site)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 3
 * 2. Obtain form data
 * 3. Stores in database
 * 4. Transistion connection from procedural to PDO Object
 * ---------------------------------------------------------------
 */

require_once('myfuncs.php');

// store registration parameters
$firstname = filter_input(INPUT_POST,'FirstName');
$lastname = filter_input(INPUT_POST,'LastName');
$username = filter_input(INPUT_POST,'UserName');
$password = filter_input(INPUT_POST,'Password');

// Validate user entry
$valid_input = true;

// Validate first name
// Note: Applying required on the html field(s) makes this unnecessary
if (is_null($firstname) || empty($firstname)) {
    $valid_input = false;
    echo "The First Name field is a required field and cannot be blank.<br />";
}

// Validate last name
if (is_null($lastname) || empty($lastname)) {
    $valid_input = false;
    echo "The Last Name field is a required field and cannot be blank.<br />";
}

// Validate user name
if (is_null($username) || empty($username)) {
    $valid_input = false;
    echo "The User Name field is a required field and cannot be blank.<br />";
}

// Validate password
if (is_null($password) || empty($password)) {
    $valid_input = false;
    echo "The Password field is a required field and cannot be blank.<br />";
}

// Check and continue only if input fields are valid 
if ($valid_input) {

    // Get Database Connection
    $db = dbConnect();
    
    // Define SQL prepare statement and bind values
    $sql = "INSERT INTO users (FIRST_NAME, LAST_NAME, USERNAME, PASSWORD) VALUES (:first, :last, :user, :pass)";
    $statement1 = $db->prepare($sql);
    $statement1->bindValue(':first', $firstname);
    $statement1->bindValue(':last', $lastname);
    $statement1->bindValue(':user', $username);
    $statement1->bindValue(':pass', $password);
    
    // Execute insert query
    $statement1->execute();
    
    echo "New records created successfully";
    
    // Close statement and connection
    $statement1->closeCursor();
    $statement1 = null;
    $db = null;
    
}

?>
