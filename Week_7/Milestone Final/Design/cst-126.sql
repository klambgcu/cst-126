-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 24, 2021 at 07:00 AM
-- Server version: 5.7.24
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cst-126`
--
CREATE DATABASE IF NOT EXISTS `cst-126` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `cst-126`;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `ID` int(11) NOT NULL,
  `CATEGORY_NAME` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`ID`, `CATEGORY_NAME`) VALUES
(2, 'Miscellaneous'),
(3, 'Programming: General'),
(5, 'Programming: Java'),
(4, 'Programming: PHP'),
(1, 'Unspecified');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `COMMENT_ID` int(11) NOT NULL,
  `POST_ID` int(11) NOT NULL,
  `COMMENT_TEXT` varchar(500) NOT NULL,
  `COMMENT_DATE` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `COMMENT_BY` int(11) NOT NULL,
  `DELETED_FLAG` varchar(1) NOT NULL DEFAULT 'n'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`COMMENT_ID`, `POST_ID`, `COMMENT_TEXT`, `COMMENT_DATE`, `COMMENT_BY`, `DELETED_FLAG`) VALUES
(1, 8, 'This is my first comment.', '2021-05-23 22:27:07', 2, 'n'),
(2, 8, 'This is the second comment on the first blog.', '2021-05-23 23:02:35', 2, 'n'),
(3, 8, 'This is the 3rd comment added.', '2021-05-23 23:53:41', 2, 'n');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `ID` int(11) NOT NULL,
  `TITLE` varchar(100) NOT NULL,
  `CATEGORY_ID` int(11) NOT NULL,
  `POST_CONTENT` text NOT NULL,
  `POSTED_DATE` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `POSTED_BY` int(11) NOT NULL,
  `UPDATED_DATE` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UPDATED_BY` int(11) NOT NULL,
  `DELETED_FLAG` varchar(1) NOT NULL DEFAULT 'n'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`ID`, `TITLE`, `CATEGORY_ID`, `POST_CONTENT`, `POSTED_DATE`, `POSTED_BY`, `UPDATED_DATE`, `UPDATED_BY`, `DELETED_FLAG`) VALUES
(8, 'Blog 1', 1, 'This is the first blog', '2021-05-16 00:00:00', 2, '2021-05-16 00:00:00', 2, 'n'),
(9, 'Blog 2', 1, 'This is a second test', '2021-05-16 00:00:00', 2, '2021-05-23 15:44:22', 2, 'n'),
(10, 'Blog 3', 2, '*** **** bad word test', '2021-05-16 00:00:00', 2, '2021-05-17 00:00:00', 2, 'n'),
(12, 'Deena Blog 1', 1, 'This is Deena\'s First Blog Updated - Edited By Admin', '2021-05-17 00:00:00', 5, '2021-05-23 15:44:35', 3, 'n');

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

DROP TABLE IF EXISTS `ratings`;
CREATE TABLE `ratings` (
  `RATING_ID` int(11) NOT NULL,
  `POST_ID` int(11) NOT NULL,
  `RATED_BY` int(11) NOT NULL,
  `RATING_VALUE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ratings`
--

INSERT INTO `ratings` (`RATING_ID`, `POST_ID`, `RATED_BY`, `RATING_VALUE`) VALUES
(2, 8, 5, 3),
(6, 8, 2, 5);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `ID` int(11) NOT NULL,
  `ROLE_NAME` varchar(50) NOT NULL,
  `ROLE_DESCRIPTION` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`ID`, `ROLE_NAME`, `ROLE_DESCRIPTION`) VALUES
(1, 'BASIC', 'Basic User: Create/Edit personal entries, Create comments on others '),
(2, 'ADVANCED', 'Advanced user: Create/Edit own posts/comments, Edit other posts/comments'),
(3, 'ADMINISTRATOR', 'Administrator: Create/Edit/Delete Posts/Comments/Users'),
(4, 'TEST ROLE', 'Test Role Creation');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `ID` int(11) NOT NULL,
  `FIRST_NAME` varchar(100) NOT NULL,
  `LAST_NAME` varchar(100) NOT NULL,
  `EMAIL` varchar(100) NOT NULL,
  `MOBILE` varchar(100) NOT NULL,
  `PASSWORD` varchar(100) NOT NULL,
  `BIRTHDATE` date NOT NULL,
  `GENDER` tinyint(1) NOT NULL,
  `ROLE_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `FIRST_NAME`, `LAST_NAME`, `EMAIL`, `MOBILE`, `PASSWORD`, `BIRTHDATE`, `GENDER`, `ROLE_ID`) VALUES
(2, 'Kelly', 'Lamb', 'daskelster@yahoo.com', '5628689956', 'Kelly123', '1965-05-01', 0, 1),
(3, 'Ad', 'min', 'admin@amazingblog.com', '8881234567', 'admin123', '2021-05-01', 0, 3),
(5, 'Deena', 'Lamb', 'dlamb@yahoo.com', '1112223333', 'Deena123', '1970-07-20', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `CATEGORY_NAME` (`CATEGORY_NAME`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`COMMENT_ID`),
  ADD KEY `COMMENTS_POST_ID` (`POST_ID`),
  ADD KEY `COMMENTS_USER_ID` (`COMMENT_BY`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `USERS_POSTS_POSTED_BY` (`POSTED_BY`) USING BTREE,
  ADD KEY `USERS_POSTS_UPDATED_BY` (`UPDATED_BY`) USING BTREE,
  ADD KEY `CATEGORY_POSTS_CATEGORY_ID` (`CATEGORY_ID`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`RATING_ID`),
  ADD KEY `RATING_POST_ID` (`POST_ID`),
  ADD KEY `RATING_USER_ID` (`RATED_BY`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ROLE_NAME` (`ROLE_NAME`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `EMAIL` (`EMAIL`),
  ADD KEY `USER_ROLES_ROLE_ID` (`ROLE_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `COMMENT_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `RATING_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`POSTED_BY`) REFERENCES `users` (`ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `posts_ibfk_2` FOREIGN KEY (`UPDATED_BY`) REFERENCES `users` (`ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `posts_ibfk_3` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `categories` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`ROLE_ID`) REFERENCES `roles` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
