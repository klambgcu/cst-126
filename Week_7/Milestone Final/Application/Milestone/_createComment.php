<!DOCTYPE html>

<!--
* ---------------------------------------------------------------
* Name      : Kelly E. Lamb
* Date      : 2021-05-23
* Class     : CST-126 Database Application Programming I
* Professor : Kondo Litchmore PhD.
* Assignment: Milestone (Blog Site)
* Disclaimer: This is my own work
* ---------------------------------------------------------------
* Description:
* 1. Create Blog Comments (_createComment.php)
* ---------------------------------------------------------------
-->

<html>
<head>
<meta charset="ISO-8859-1">
<link rel=stylesheet href="css/main_nav.css" />
<title>Amazing Blog Site - Create Comment</title>
</head>
<body>

<?php require_once('util_funcs.php');?>
<?php require_once '_main_menu.php';?>


	<div align="center">
    	<hr><br />
    	<h1>Amazing Blog Site!</h1>
    	<hr><br />
    	<div align="center">
    		<form action="commentCreateHandler.php" method="POST">
    		    <h1>Create Comment</h1>
    		    <p>Add a comment and click submit.</p>
    		    <hr><br />
    		    Author: <?php echo $blog_row[0]['FIRST_NAME'] . " " . $blog_row[0]['LAST_NAME']; ?><br /><br />
    		    Created on: <?php $date=date_create($blog_row[0]['POSTED_DATE']); echo date_format($date,"Y-m-d"); ?><br /><br />

<?php
        echo "    		    Category: " . $blog_row[0]['CATEGORY_NAME'] . "<br /><br />\n";
        echo "    			<input type=\"hidden\" id=\"BlogCategoryID\" name=\"BlogCategoryID\" value=\"" . $blog_row[0]['CATEGORY_ID'] . "\">\n";
?>

    		    <div id="error_message">
					<?php if(!empty($_SESSION['errMsg'])) { echo $_SESSION['errMsg'] . "<br />"; unset($_SESSION['errMsg']); } ?>
    			</div>
    		    <label for="BlogTitle"><b>Title:</b></label>
    		    <input type="text" placeholder="Enter a Blog Title" name="BlogTitle" id="BlogTitle" readonly <?php echo "value='" . $blog_row[0]['TITLE'] . "'"; ?>><br /><br />

    		    <label for="BlogContent"><b>Blog Content:</b></label>
    		    <textarea name="BlogContent" id = "BlogContent" rows="5" cols="100" maxlength="500" readonly><?php echo $blog_row[0]['POST_CONTENT']; ?></textarea>
    			<br /><br />

    		    <label for="CommentContent"><b>Add a comment:</b></label>
    		    <textarea name="CommentContent" id = "CommentContent" rows="5" cols="100" maxlength="500" required></textarea>
    			<br /><br />

    			<input type="hidden" id="BlogID" name="BlogID" <?php echo 'value="' . $blog_row[0]['ID'] . '"';?>>

    		    <button type="submit">Submit</button><br /><br />
    		    <hr>
    		</form>
		</div>
	</div>

</body>
</html>
