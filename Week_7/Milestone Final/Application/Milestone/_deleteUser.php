<!DOCTYPE html>

<!--
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-16
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone (Blog Site)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Delete User (_deleteUser.php)
 * 2. Simple place holder
 * 3. TO DO: Add card/image/detail
 * ---------------------------------------------------------------
 -->

<html>
<head>
<meta charset="ISO-8859-1">
<link rel=stylesheet href="css/main_nav.css" />
<title>Amazing Blog Site - Delete User</title>
</head>
<body>

<?php require_once('util_funcs.php');?>
<?php require_once '_main_menu.php';?>


	<div align="center">
    	<hr><br />
    	<h1>Amazing Blog Site!</h1>
    	<hr><br />
    	<div align="center">
		    <h1>User Delete (DISABLED FOR NOW)</h1>
		    <p>Confirm deletion by clicking on confirm.</p>
		    <hr><br />
 		</div>
	</div>

</body>
</html>
