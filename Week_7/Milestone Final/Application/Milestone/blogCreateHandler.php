<?php
session_start();

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-02
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone (Blog Site)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Blog Creation Handler (blogCreateHandler.php)
 * 2. Retrieves fields from blogCreate.php
 * 3. Stores in database
 * ---------------------------------------------------------------
 */

require_once('util_funcs.php');
require_once('filterWords.php');

// store registration parameters - censor 'bad words'
$blogTitle   = filterwords( filter_input(INPUT_POST,'BlogTitle') );
$blogContent = filterwords( filter_input(INPUT_POST,'BlogContent') );
$categoryId  = 1; // Default to unspecified - Admin Chooses

// Get user id from session
$user_info = getUserInfo();
$userID = $user_info[0]['ID'];

// Create create/update date for insert - "YYYY-MM-DD" format
$cudate = date("Y-m-d");

try
{
    // Get Database Connection
    $db = dbConnect();

    // Title must be unique
    $sql = "SELECT * FROM posts WHERE TITLE = :blogTitle";
    $statement = $db->prepare($sql);
    $statement->bindValue(':blogTitle',   $blogTitle);
    $statement->execute();
    $row = $statement->fetchAll(\PDO::FETCH_ASSOC);
    $num_rows = count($row);

    $statement->closeCursor();
    $statement = null;

    if ($num_rows == 0)
    {
        // Define SQL prepare statement and bind values
        $sql = "INSERT INTO posts (TITLE, CATEGORY_ID, POST_CONTENT, POSTED_DATE, POSTED_BY, UPDATED_DATE, UPDATED_BY) " .
               "VALUES (:blogTitle, :categoryId, :blogContent, :pDate, :pUserID, :uDate, :uUserID)";

        $statement1 = $db->prepare($sql);
        $statement1->bindValue(':blogTitle',   $blogTitle);
        $statement1->bindValue(':categoryId',  $categoryId);
        $statement1->bindValue(':blogContent', $blogContent);
        $statement1->bindValue(':pDate',       $cudate);
        $statement1->bindValue(':pUserID',     $userID);
        $statement1->bindValue(':uDate',       $cudate);
        $statement1->bindValue(':uUserID',     $userID);

        // Execute insert query
        $statement1->execute();
    }
    else
    {
        $db = null;
        $_SESSION['errMsg'] = "Title Must Be Unique.";
        header('Location: blogCreate.php');
        exit();
    }
} catch (PDOException $e)
{
    $error_message = $e->getMessage();
    include('database_error.php');
    exit();
}

// Close statement and connection
$statement1->closeCursor();
$statement1 = null;
$db = null;

header('Location: index.php');

?>
