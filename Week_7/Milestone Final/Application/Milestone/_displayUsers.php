<!--
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-16
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone 5
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone (_displayUsers.php) display table of users for edit
 * 2. Reusable functions
 * ---------------------------------------------------------------
 -->

<table id="post_entries">
    <tr>
        <th>ID</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Email</th>
        <th>Mobile</th>
        <th>Password</th>
        <th>Birthdate</th>
        <th>Gender</th>
        <th>Role</th>
        <th>Action</th>
    </tr>

<?php
    $gender = array();
    $gender[0] = "Male";
    $gender[1] = "Female";

    for($x=0;$x < count($users); $x++)
    {
        echo "  <tr>\n";
        echo "      <td>" . $users[$x][0] . "</td>\n";
        echo "      <td>" . $users[$x][1] . "</td>\n";
        echo "      <td>" . $users[$x][2] . "</td>\n";
        echo "      <td>" . $users[$x][3] . "</td>\n";
        echo "      <td>" . $users[$x][4] . "</td>\n";
        echo "      <td>" . $users[$x][5] . "</td>\n";
        echo "      <td>" . $users[$x][6] . "</td>\n";
        echo "      <td>" . $gender[$users[$x][7]] . "</td>\n";
        echo "      <td>" . $users[$x][9] . "</td>\n";
        echo "      <td>" . "<a href=userChangeHandler.php?id=" . $users[$x][0] . "&mode=0>Edit<a>" . "</td>\n";
        echo "  </tr>\n";
	}
 ?>

</table>
