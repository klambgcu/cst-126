<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-04-26
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone (Blog Site)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 3
 * 2. Obtain form data
 * 3. Handle Login Validation
 * 4. Transistion from procedural connection to PDO object
 * ---------------------------------------------------------------
 */

?>

<!DOCTYPE html>
<html>
	<!--  the head section -->
	<head>
		<title>Login Failed</title>
	</head>

	<!-- the body section -->
	<body>
	<main>
		<h1>Login Failed</h1>
		<h2><?php echo $message?></h2>
		<br />
		<br />
		<a href='index.php'>Welcome Page</a>
		<br />";
	</main>
	</body>


</html>