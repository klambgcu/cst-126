<!DOCTYPE html>

<!--
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-16
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone (Blog Site)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Edit Posts (_deleteBlog.php)
 * 2. Simple place holder
 * 3. TO DO: Add card/image/detail
 * ---------------------------------------------------------------
 -->

<html>
<head>
<meta charset="ISO-8859-1">
<link rel=stylesheet href="css/main_nav.css" />
<title>Amazing Blog Site - Delete Post</title>
</head>
<body>

<?php require_once('util_funcs.php');?>
<?php require_once '_main_menu.php';?>


	<div align="center">
    	<hr><br />
    	<h1>Amazing Blog Site!</h1>
    	<hr><br />
    	<div align="center">
    		<form action="blogDeleteHandler.php" method="POST">
    		    <h1>Blog Delete</h1>
    		    <p>Confirm deletion by clicking on confirm.</p>
    		    <hr><br />
    		    Author: <?php echo $blog_row[0]['FIRST_NAME'] . " " . $blog_row[0]['LAST_NAME']; ?><br /><br />
    		    Created on: <?php $date=date_create($blog_row[0]['POSTED_DATE']); echo date_format($date,"Y-m-d"); ?><br /><br />

<?php
        echo "    		    Category: " . $blog_row[0]['CATEGORY_NAME'] . "<br /><br />\n";
        echo "    			<input type=\"hidden\" id=\"BlogCategoryID\" name=\"BlogCategoryID\" value=\"" . $blog_row[0]['CATEGORY_ID'] . "\">\n";
?>

    		    <div id="error_message">
					<?php if(!empty($_SESSION['errMsg'])) { echo $_SESSION['errMsg'] . "<br />"; unset($_SESSION['errMsg']); } ?>
    			</div>
    		    <label for="BlogTitle"><b>Title:</b></label>
    		    <input type="text" placeholder="Enter a Blog Title" name="BlogTitle" id="BlogTitle" readonly <?php echo "value='" . $blog_row[0]['TITLE'] . "'"; ?>><br /><br />

    		    <label for="BlogContent"><b>Blog Content:</b></label>
    		    <textarea name="BlogContent" id = "BlogContent" rows="5" cols="100" maxlength="500" readonly><?php echo $blog_row[0]['POST_CONTENT']; ?></textarea>
    			<br /><br />
    			<input type="hidden" id="BlogID" name="BlogID" <?php echo 'value="' . $blog_row[0]['ID'] . '"';?>>
    		    <button type="submit">Confirm Delete</button><br /><br />
    		    <hr>
    		</form>
		</div>
	</div>

</body>
</html>
