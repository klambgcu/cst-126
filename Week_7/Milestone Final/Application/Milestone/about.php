<!DOCTYPE html>

<!--
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-02
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone (Blog Site)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. About Page (about.php)
 * 2. Simple place holder
 * 3. TO DO: Add card/image/detail
 * ---------------------------------------------------------------
 -->

<html>
<head>
<meta charset="ISO-8859-1">
<link rel=stylesheet href="css/main_nav.css" />
<title>Amazing Blog Site - About</title>
</head>
<body>

<?php require_once '_main_menu.php';?>

	<div align="center">
    	<hr><br />
    	<h1>Amazing Blog Site!</h1>
    	<hr><br />
    	<h1>About</h1>
    	<h2>Name: Kelly Lamb</h2>
    	<h3>Course: CST-126 Database Application Programming I</h3>
    	<h3>Professor : Kondo Litchmore PhD.</h3>
	</div>

</body>
</html>