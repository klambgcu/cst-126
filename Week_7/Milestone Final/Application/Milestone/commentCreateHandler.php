<?php
session_start();

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-23
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone (Blog Site)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Blog Comment Creation Handler (commentCreateHandler.php)
 * 2. Retrieves fields from _createComment.php
 * 3. Stores in database
 * ---------------------------------------------------------------
 */

require_once('util_funcs.php');
require_once('filterWords.php');

// store registration parameters - censor 'bad words'
$commentContent = filterwords( filter_input(INPUT_POST,'CommentContent') );
$blogID         = filterwords( filter_input(INPUT_POST,'BlogID') );

// Get user id from session
$user_info = getUserInfo();
$userID = $user_info[0]['ID'];

try
{
    // Get Database Connection
    $db = dbConnect();

    // Define SQL prepare statement and bind values
    $sql = "INSERT INTO comments (POST_ID, COMMENT_TEXT, COMMENT_BY) VALUES ( :blogID, :comment, :userID) ";

    $statement1 = $db->prepare($sql);
    $statement1->bindValue(':blogID',  $blogID);
    $statement1->bindValue(':comment', $commentContent);
    $statement1->bindValue(':userID',  $userID);
    // Execute update query
    $statement1->execute();

} catch (PDOException $e)
{
    $error_message = $e->getMessage();
    include('database_error.php');
    exit();
}

// Close statement and connection
$statement1->closeCursor();
$statement1 = null;
$db = null;

header('Location: index.php');

?>