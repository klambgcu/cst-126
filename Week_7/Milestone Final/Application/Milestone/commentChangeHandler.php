<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-23
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone (Blog Site)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone - Handle Comment Change
 * 2. Obtain form data
 * 3. Handle Login Validation
 * ---------------------------------------------------------------
 */

require_once('util_funcs.php');

// store registration parameters
$blog_id = filter_input(INPUT_GET,'id');
$mode    = filter_input(INPUT_GET,'mode'); // 0 - Create

// Validate mode operations
if ( ($mode < 0) || ($mode > 0) )
{
    echo "Invalid Request Operation - Contact Administrator.<br />";
    exit();
}

// Get data to pass to user interface for edit/delete
$blog_row = getBlogByID($blog_id);
$categories = getAllCategories();

include('_createComment.php');


?>


