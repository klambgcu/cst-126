<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-16
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone (Blog Site)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone - Handle Post Edit / Delete Requests
 * 2. Obtain form data
 * 3. Handle Login Validation
 * ---------------------------------------------------------------
 */

require_once('util_funcs.php');

// store registration parameters
$blog_id = filter_input(INPUT_GET,'id');
$mode    = filter_input(INPUT_GET,'mode'); // 0 - Edit, 1 - Delete

// Validate mode operations
if ( ($mode < 0) || ($mode > 1) )
{
    echo "Invalid Request Operation - Contact Administrator.<br />";
    exit();
}

// Get data to pass to user interface for edit/delete
$blog_row = getBlogByID($blog_id);
$categories = getAllCategories();

if ($mode == 0)
{
    include('_editBlog.php');
}
else
{
    include('_deleteBlog.php');
}
?>