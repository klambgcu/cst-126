<!DOCTYPE html>

<!--
* ---------------------------------------------------------------
* Name      : Kelly E. Lamb
* Date      : 2021-05-23
* Class     : CST-126 Database Application Programming I
* Professor : Kondo Litchmore PhD.
* Assignment: Milestone (Blog Site)
* Disclaimer: This is my own work
* ---------------------------------------------------------------
* Description:
* 1. Edit Posts (_createRating.php)
* ---------------------------------------------------------------
-->

<html>
<head>
<meta charset="ISO-8859-1">
<link rel=stylesheet href="css/main_nav.css" />
<title>Amazing Blog Site - Add Rating</title>
</head>
<body>

<?php require_once('util_funcs.php');?>
<?php require_once '_main_menu.php';?>


	<div align="center">
    	<hr><br />
    	<h1>Amazing Blog Site!</h1>
    	<hr><br />
    	<div align="center">
    		<form action="ratingCreateHandler.php" method="POST">
    		    <h1>Add Rating</h1>
    		    <p>Choose a rating (1-5).</p>
    		    <hr><br />
    		    Author: <?php echo $blog_row[0]['FIRST_NAME'] . " " . $blog_row[0]['LAST_NAME']; ?><br /><br />
    		    Created on: <?php $date=date_create($blog_row[0]['POSTED_DATE']); echo date_format($date,"Y-m-d"); ?><br /><br />

<?php
        echo "    		    Category: " . $blog_row[0]['CATEGORY_NAME'] . "<br /><br />\n";
        echo "    			<input type=\"hidden\" id=\"BlogCategoryID\" name=\"BlogCategoryID\" value=\"" . $blog_row[0]['CATEGORY_ID'] . "\">\n";
?>

    		    <div id="error_message">
					<?php if(!empty($_SESSION['errMsg'])) { echo $_SESSION['errMsg'] . "<br />"; unset($_SESSION['errMsg']); } ?>
    			</div>
    		    <label for="BlogTitle"><b>Title:</b></label>
    		    <input type="text" placeholder="Enter a Blog Title" name="BlogTitle" id="BlogTitle" readonly <?php echo "value='" . $blog_row[0]['TITLE'] . "'"; ?>><br /><br />

    		    <label for="BlogContent"><b>Blog Content:</b></label>
    		    <textarea name="BlogContent" id = "BlogContent" rows="5" cols="100" maxlength="500" readonly><?php echo $blog_row[0]['POST_CONTENT']; ?></textarea>
    			<br /><br />

                <label for="Rating"><b>Rating:</b></label>
                <select name="Rating" id="Rating" required>

<?php

echo "RATING = [" . $rating . "]";
if ($rating == 0)
    echo "                    <option value='0' selected>No Rating</option>\n";
else
    echo "                    <option value='0'>No Rating</option>\n";

if ($rating == 1)
    echo "                    <option value='1' selected>1-Star</option>\n";
else
    echo "                    <option value='1'>1-Star</option>\n";

if ($rating == 2)
    echo "                    <option value='2' selected>2-Star</option>\n";
else
    echo "                    <option value='2'>2-Star</option>\n";

if ($rating == 3)
    echo "                    <option value='3' selected>3-Star</option>\n";
else
    echo "                    <option value='3'>3-Star</option>\n";

if ($rating == 4)
    echo "                    <option value='4' selected>4-Star</option>\n";
else
    echo "                    <option value='4'>4-Star</option>\n";

if ($rating == 5)
    echo "                    <option value='5' selected>5-Star</option>\n";
else
    echo "                    <option value='5'>5-Star</option>\n";

?>
                </select><br /><br />

    			<input type="hidden" id="BlogID" name="BlogID" <?php echo 'value="' . $blog_row[0]['ID'] . '"';?>>

    		    <button type="submit">Submit</button><br /><br />
    		    <hr>
    		</form>
		</div>
	</div>

</body>
</html>
