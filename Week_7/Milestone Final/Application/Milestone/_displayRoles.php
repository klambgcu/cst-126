<?php require_once 'util_funcs.php' ?>

<!--
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-16
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone 6
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone - display table of existing roles
 * 2. Reusable functions
 * ---------------------------------------------------------------
 -->

<table id="post_entries">
    <tr>
        <th>ID</th>
        <th>Role Name</th>
        <th>Role Description</th>
    <tr>

<?php


    for($x=0; $x < count($roles); $x++)
    {
        echo "  <tr>\n";
        echo "      <td>" . $roles[$x][0] . "</td>\n";
        echo "      <td>" . $roles[$x][1] . "</td>\n";
        echo "      <td>" . $roles[$x][2] . "</td>\n";
        echo "  </tr>\n";
	}
 ?>

</table>
