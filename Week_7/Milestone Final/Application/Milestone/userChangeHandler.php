<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-16
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone (Blog Site)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone - Handle Post Edit / Delete Requests
 * 2. Obtain form data
 * 3. Handle Login Validation
 * ---------------------------------------------------------------
 */

require_once('util_funcs.php');

// store registration parameters
$user_id = filter_input(INPUT_GET,'id');
$mode    = filter_input(INPUT_GET,'mode'); // 0 - Edit, 1 - Delete (Disable for now)

// Validate mode operations
if ( ($mode < 0) || ($mode >= 1) )
{
    echo "Invalid Request Operation - Contact Administrator.<br />";
    exit();
}

try
{
    // Get Database Connection
    $db = dbConnect();

    // Define SQL prepare statement and bind values
    $sql = "SELECT u.*, r.ROLE_NAME " .
           "  FROM users u, roles r " .
           " WHERE u.ID = :user_id AND u.ROLE_ID = r.ID ";
    $statement1 = $db->prepare($sql);
    $statement1->bindValue(':user_id', $user_id);

    // Execute query
    $statement1->execute();
    $user_row = $statement1->fetchAll(\PDO::FETCH_ASSOC);

    // Define SQL prepare statement and bind values
    $sql = "SELECT * FROM roles ORDER BY ID";
    $statement1 = $db->prepare($sql);

    // Execute query
    $statement1->execute();
    $roles = $statement1->fetchAll(\PDO::FETCH_ASSOC);

} catch(PDOException $e)
{
    $error_message = $e->getMessage();
    include('database_error.php');
    exit();
}

// Close statement and connection
$statement1->closeCursor();
$statement1 = null;
$db = null;

if ($mode == 0)
{
    include('_editUser.php');
}
else
{
    include('_deleteUser.php');
}
?>