<!DOCTYPE html>

<!--
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-04-19
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone (Blog Site)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Welcome / Default Page (index.html)
 * 2. Initial Menu Options for home, login, register
 * 3.
 * ---------------------------------------------------------------
 -->

<html>
<head>
<meta charset="ISO-8859-1">
<link rel=stylesheet href="css/main_nav.css" />
<link rel=stylesheet href="css/post_entries.css" />
<title>Amazing Blog Site</title>
</head>
<body>

<?php require_once 'util_funcs.php' ?>
<?php require_once '_main_menu.php';?>

	<div align="center">
    	<hr><br />
    	<h1>Welcome to the Amazing Blog Site!</h1>
    	<hr><br />
	</div>
<?php
    // Check if user is logged in - display blogs
    $user_info = getUserInfo();

    if (isset($user_info))
    {
        $search_info = getSearchInfo();

        echo "        <div align=\"center\">\n";
        echo "            <form action=\"blogSearchHandler.php\" method=\"POST\">\n";
        echo "                <h3>Search Filters</h3>\n";
        echo "                <hr><br />\n";
        echo "                <label for=\"SearchTitle\"><b>Title:</b></label>\n";
        echo "                <input type=\"text\" placeholder=\"Enter a Blog Title\" name=\"SearchTitle\" id=\"SearchTitle\" value=\"" . $search_info[0] . "\"><br /><br />\n";
        echo "                <label for=\"SearchContent\"><b>Content:</b></label>\n";
        echo "                <input type=\"text\" placeholder=\"Enter Blog Content\" name=\"SearchContent\" id=\"SearchContent\" value=\"" . $search_info[1] . "\"><br /><br />\n";
        echo "                <br />\n";
        echo "                <button type=\"submit\">Submit</button><br /><br />\n";
        echo "                <hr>\n";
        echo "            </form>\n";
        echo "        </div>\n";
        $blogs = getAllBlogs();
        include('_displayBlogs.php');
    }
?>

</body>
</html>