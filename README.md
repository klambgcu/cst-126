# README #

Kelly Lamb


### What is this repository for? ###

GCU Course Work: CST-126 Database Application Programming I

### How do I get set up? ###

* Download Latest versions of MAMP, Eclipse PHP, OpenJDK (current 16)
* Setup Environment PATH, CLASSPATH, JAVA_HOME
* Configure Eclipse.ini
-vm
C:\Program Files\Java\jdk-16\bin
-vmargs

* Database configuration (PHPMyAdmin-MySQL - various databases/tables/etc.)
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin: Kelly Lamb
