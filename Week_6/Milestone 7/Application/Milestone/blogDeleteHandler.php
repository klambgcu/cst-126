<?php
session_start();

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-16
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone (Blog Site)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Blog Creation Handler (blogDeleteHandler.php)
 * 2. Retrieves fields from _blogDelete.php
 * 3. Stores in database
 * ---------------------------------------------------------------
 */

require_once('util_funcs.php');
require_once('filterWords.php');

// store registration parameters - censor 'bad words'
$blogID = filterwords( filter_input(INPUT_POST,'BlogID') );

try
{
    // Get Database Connection
    $db = dbConnect();
        
    // Define SQL prepare statement and bind values
    $sql = "UPDATE posts SET DELETED_FLAG = 'y' WHERE ID = :blogID";
    
    $statement1 = $db->prepare($sql);
    $statement1->bindValue(':blogID',      $blogID);
    // Execute update query
    $statement1->execute();

} catch (PDOException $e)
{
    $error_message = $e->getMessage();
    include('database_error.php');
    exit();
}

// Close statement and connection
$statement1->closeCursor();
$statement1 = null;
$db = null;

header('Location: index.php');

?>
