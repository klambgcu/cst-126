<?php
session_start();

/* 
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-16
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone 6
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone - display table of existing roles
 * 2. Reusable functions
 * ---------------------------------------------------------------
 */

require_once('util_funcs.php');

// store registration parameters
$roleName        = filter_input(INPUT_POST,'RoleName');
$roleDescription = filter_input(INPUT_POST,'RoleDescription');

try
{
    // Get Database Connection
    $db = dbConnect();

    // Title must be unique
    $sql = "SELECT * FROM roles WHERE ROLE_NAME = :roleName";
    $statement = $db->prepare($sql);
    $statement->bindValue(':roleName', $roleName);
    $statement->execute();
    $row = $statement->fetchAll(\PDO::FETCH_ASSOC);
    $num_rows = count($row);
    
    $statement->closeCursor();
    $statement = null;
    
    if ($num_rows == 0)
    {
        // Define SQL prepare statement and bind values
        $sql = "INSERT INTO roles (ROLE_NAME, ROLE_DESCRIPTION) " .
               "VALUES (:roleName, :roleDescription)";
        
        $statement1 = $db->prepare($sql);
        $statement1->bindValue(':roleName',        $roleName);
        $statement1->bindValue(':roleDescription', $roleDescription);
        
        // Execute insert query
        $statement1->execute();
    }
    else 
    {
        $db = null;
        $_SESSION['errMsg'] = "Role Name Must Be Unique.";
        header('Location: admin_create_role.php');
        exit();
    }
} catch (PDOException $e)
{
    $error_message = $e->getMessage();
    include('database_error.php');
    exit();
}

// Close statement and connection
$statement1->closeCursor();
$statement1 = null;
$db = null;

header('Location: admin_create_role.php');

?>