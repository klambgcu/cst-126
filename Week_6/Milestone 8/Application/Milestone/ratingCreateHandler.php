<?php
session_start();

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-23
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone (Blog Site)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Blog Rating Creation Handler (ratingCreateHandler.php)
 * 2. Retrieves fields from _createRating.php
 * 3. Stores in database
 * ---------------------------------------------------------------
 */

require_once('util_funcs.php');

// store registration parameters - censor 'bad words'
$rating = filter_input(INPUT_POST,'Rating');
$blogID = filter_input(INPUT_POST,'BlogID');

// Get user id from session
$user_info = getUserInfo();
$userID = $user_info[0]['ID'];

try
{
    // Get Database Connection
    $db = dbConnect();
    
    $sql = "DELETE FROM ratings WHERE POST_ID = :blogID AND RATED_BY = :userID ";
    $statement1 = $db->prepare($sql);
    $statement1->bindValue(':blogID',  $blogID);
    $statement1->bindValue(':userID',  $userID);
    // Execute update query
    $statement1->execute();
    
    // Define SQL prepare statement and bind values
    $sql = "INSERT INTO ratings (POST_ID, RATED_BY, RATING_VALUE) VALUES ( :blogID, :userID, :rating ) ";
    $statement1 = $db->prepare($sql);
    $statement1->bindValue(':blogID',  $blogID);
    $statement1->bindValue(':userID',  $userID);
    $statement1->bindValue(':rating',  $rating);
    // Execute update query
    $statement1->execute();

} catch (PDOException $e)
{
    $error_message = $e->getMessage();
    include('database_error.php');
    exit();
}

// Close statement and connection
$statement1->closeCursor();
$statement1 = null;
$db = null;

header('Location: index.php');

?>