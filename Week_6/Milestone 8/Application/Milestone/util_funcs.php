<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-23
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. util_funcs.php - a collection of functions
 * 2.
 * 3.
 * 4.
 * ---------------------------------------------------------------
 */

//
// Create a database PDO connection
// Returns the connection
// Throw exception to database_error display form
//
function dbConnect() {
    
    // Define local / development database connection parameters
    $connect_string = 'mysql:host=localhost:3306;dbname=cst-126';
    $db_username = "root";
    $db_password = "root";

    // Define azure / publish database connection parameters
    // $connect_string = 'mysql:host=127.0.0.1:49921;dbname=cst-126';
    // $db_username = "azure";
    // $db_password = "6#vWHD_$";
    
    try
    {
        // Create a PDO object
        $db_connection = new PDO($connect_string, $db_username, $db_password);
        $db_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $e)
    {
        $error_message = $e->getMessage();
        include('database_error.php');
        exit();
    }
    
    return $db_connection;
}

function getAllUsers()
{
    // Get Database Connection
    $db = dbConnect();
 
    // Define SQL prepare statement and bind values
    $sql = "SELECT u.*, r.ROLE_NAME " . 
           "  FROM users u, roles r " . 
           " WHERE u.ROLE_ID = r.ID " . 
           " ORDER BY u.LAST_NAME, u.FIRST_NAME";
    $statement1 = $db->prepare($sql);
    
    // Execute select query
    $statement1->execute();
    
    // return records as associative array - could use fetchAll
    $statement1->setFetchMode(PDO::FETCH_ASSOC);
    
    // add records to users array
    $users = array();
    $index = 0;
    while ($row = $statement1->fetch())
    {
        $users[$index] = array($row["ID"], $row["FIRST_NAME"], $row["LAST_NAME"], $row["EMAIL"], $row["MOBILE"], $row["PASSWORD"], $row["BIRTHDATE"], $row["GENDER"], $row["ROLE_ID"], $row["ROLE_NAME"]);
        ++$index;
    }
    
    // Close statement and connection
    $statement1->closeCursor();
    $statement1 = null;
    $db = null;
    
    return $users;
}

function getAllCategories()
{
    // Get Database Connection
    $db = dbConnect();
    
    // Define SQL prepare statement and bind values
    $sql = "SELECT * " .
           "  FROM categories " .
           " ORDER BY ID ";
    $statement1 = $db->prepare($sql);
    
    // Execute select query
    $statement1->execute();
    
    // return records as associative array - could use fetchAll
    $statement1->setFetchMode(PDO::FETCH_ASSOC);
    
    // add records to users array
    $categories = array();
    $index = 0;
    while ($row = $statement1->fetch())
    {
        $categories[$index] = array($row["ID"], $row["CATEGORY_NAME"]);
        ++$index;
    }
    
    // Close statement and connection
    $statement1->closeCursor();
    $statement1 = null;
    $db = null;
    
    return $categories;
}

function getAllRoles()
{
    // Get Database Connection
    $db = dbConnect();
    
    // Define SQL prepare statement and bind values
    $sql = "SELECT * " .
           "  FROM roles " .
           " ORDER BY ID";
    $statement1 = $db->prepare($sql);
    
    // Execute select query
    $statement1->execute();
    
    // return records as associative array - could use fetchAll
    $statement1->setFetchMode(PDO::FETCH_ASSOC);
    
    // add records to users array
    $roles = array();
    $index = 0;
    while ($row = $statement1->fetch())
    {
        $roles[$index] = array($row["ID"], $row["ROLE_NAME"], $row["ROLE_DESCRIPTION"]);
        ++$index;
    }
    
    // Close statement and connection
    $statement1->closeCursor();
    $statement1 = null;
    $db = null;
    
    return $roles;
}

function getRatingByPostUserID($post_id, $user_id)
{
    try
    {
        // Get Database Connection
        $db = dbConnect();
        
        // Define SQL prepare statement and bind values
        $sql = "SELECT COALESCE(RATING_VALUE, 0) AS RATING_VALUE " .
               "  FROM ratings " .
               " WHERE POST_ID = :post_id AND RATED_BY = :user_id ";
        $statement1 = $db->prepare($sql);
        $statement1->bindValue(':post_id', $post_id);
        $statement1->bindValue(':user_id', $user_id);
        
        // Execute select query
        $statement1->execute();
        
        // return records as associative array - could use fetchAll
        $statement1->setFetchMode(PDO::FETCH_ASSOC);
        $row = $statement1->fetchAll(\PDO::FETCH_ASSOC);
        $num_rows = count($row);
         
        $rating = 0;
        if ($num_rows > 0)
        {
            $rating = $row[0]['RATING_VALUE'];
        }
        
        // Close statement and connection
        $statement1->closeCursor();
        $statement1 = null;
        $db = null;
        
        return $rating;
    
    } catch(PDOException $e)
    {
        $error_message = $e->getMessage();
        include('database_error.php');
        exit();
    }
}

function getBlogByID($blog_id)
{
    try
    {
        // Get Database Connection
        $db = dbConnect();
        
        // Define SQL prepare statement and bind values
        $sql = "SELECT p.*, u.FIRST_NAME, u.LAST_NAME, c.CATEGORY_NAME FROM posts p, users u, categories c WHERE p.ID = :blog_id AND p.POSTED_BY = u.ID AND p.CATEGORY_ID = c.ID";
        $statement1 = $db->prepare($sql);
        $statement1->bindValue(':blog_id', $blog_id);
        
        // Execute query
        $statement1->execute();
        $blog_row = $statement1->fetchAll(\PDO::FETCH_ASSOC);
        
        // Close statement and connection
        $statement1->closeCursor();
        $statement1 = null;
        $db = null;
        
        return $blog_row;
        
    } catch(PDOException $e)
    {
        $error_message = $e->getMessage();
        include('database_error.php');
        exit();
    }
}

function getAllCommentsByID($blog_id)
{
    try
    {
        // Get Database Connection
        $db = dbConnect();
        
        // Define SQL prepare statement and bind values
        $sql = "SELECT * " .
               "  FROM comments " .
               " WHERE POST_ID = :blog_id " .
               " ORDER BY COMMENT_ID ";
        $statement1 = $db->prepare($sql);
        $statement1->bindValue(':blog_id', $blog_id);
        
        // Execute query
        $statement1->execute();
        $comments = $statement1->fetchAll(\PDO::FETCH_ASSOC);
        
        // Close statement and connection
        $statement1->closeCursor();
        $statement1 = null;
        $db = null;
        
        return $comments;
        
    } catch(PDOException $e)
    {
        $error_message = $e->getMessage();
        include('database_error.php');
        exit();
    }
}

function getAllBlogs()
{
    // Get Database Connection
    $db = dbConnect();
    
    // get search filter criteria
    $search_info = getSearchInfo();
    
    // Define SQL prepare statement and bind values
    $sql = "SELECT p.*, u.FIRST_NAME, u.LAST_NAME, c.CATEGORY_NAME, (SELECT COALESCE(AVG(r.RATING_VALUE),0) FROM ratings r where p.id = r.POST_ID) AS RATING_AVG " .
           " FROM posts p, categories c, users u " .
           " WHERE p.DELETED_FLAG = 'n' AND p.CATEGORY_ID = c.ID AND p.POSTED_BY = u.ID" .
           " AND (p.TITLE LIKE :title AND p.POST_CONTENT LIKE :content)";
    $statement1 = $db->prepare($sql);
    $statement1->bindValue(':title',   "%" . $search_info[0] . "%");
    $statement1->bindValue(':content', "%" . $search_info[1] . "%");
    
    // Execute select query
    $statement1->execute();
    
    // return records as associative array - could use fetchAll
    $statement1->setFetchMode(PDO::FETCH_ASSOC);
    
    // add records to blogs array
    $blogs = array();
    $index = 0;
    while ($row = $statement1->fetch())
    {
        $blogs[$index] = array($row["ID"],          $row["TITLE"],         $row["CATEGORY_ID"],  $row["POST_CONTENT"],
                               $row["POSTED_DATE"], $row["POSTED_BY"],     $row["UPDATED_DATE"], $row["UPDATED_BY"],
                               $row["CATEGORY_ID"], $row["CATEGORY_NAME"], $row["FIRST_NAME"],   $row["LAST_NAME"],
                               $row["RATING_AVG"] );
        ++$index;
    }
    
    // Close statement and connection
    $statement1->closeCursor();
    $statement1 = null;
    $db = null;
    
    return $blogs;
}

function saveUserId($id)
{
    session_start();
    $_SESSION["USER_ID"] = $id;
}

function getUserId()
{
    session_start();
    return $_SESSION["USER_ID"];
}

function saveUserFirstName($firstName)
{
    session_start();
    $_SESSION["USER_FIRSTNAME"] = $firstName;
}

function getUserFirstName()
{
    session_start();
    return $_SESSION["USER_FIRSTNAME"];
}

function saveUserLasttName($lastName)
{
    session_start();
    $_SESSION["USER_LASTNAME"] = $lastName;
}

function getUserLastName()
{
    session_start();
    return $_SESSION["USER_LASTNAME"];
}

function saveUserInfo($user_info_array)
{
    session_start();
    $_SESSION["USER_INFO"] = $user_info_array;
}

function getUserInfo()
{
    session_start();
    return $_SESSION["USER_INFO"];
}

function saveUserRoleId($role_id)
{
    session_start();
    return $_SESSION["ROLE_ID"];
}

function getUserRoleId()
{
    session_start();
    return $_SESSION["ROLE_ID"];
}

function saveSearchInfo($search_info_array)
{
    session_start();
    $_SESSION["SEARCH_INFO"] = $search_info_array;
}

function getSearchInfo()
{
    session_start();
    return $_SESSION["SEARCH_INFO"];
}

?>