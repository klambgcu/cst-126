<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-23
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone (Blog Site)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone - Handle Rating Change
 * 2. Obtain form data
 * 3. 
 * ---------------------------------------------------------------
 */

require_once('util_funcs.php');

// store registration parameters
$blog_id = filter_input(INPUT_GET,'id');
$mode    = filter_input(INPUT_GET,'mode'); // 0 - Create

// Get user id from session
$user_info = getUserInfo();
$user_id = $user_info[0]['ID'];

// Validate mode operations
if ( ($mode < 0) || ($mode > 0) )
{
    echo "Invalid Request Operation - Contact Administrator.<br />";
    exit();
}

// Get data to pass to user interface for edit/delete
$blog_row = getBlogByID($blog_id);
$categories = getAllCategories();
$rating = getRatingByPostUserID($blog_id, $user_id);

include('_createRating.php');

?>