<?php
session_start();

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-23
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone (Blog Site)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Blog Search Handler (blogSearchHandler.php)
 * 2. Retrieves fields from index.php
 * 3. Creates session array search_info - useful in index.php
 * ---------------------------------------------------------------
 */

require_once('util_funcs.php');

$blogSearchTitle   =  filter_input(INPUT_POST,'SearchTitle');
$blogSearchContent =  filter_input(INPUT_POST,'SearchContent');

if (is_null($blogSearchTitle) || empty($blogSearchTitle))
{
    $blogSearchTitle = ""; // default to nothing for query
}

if (is_null($blogSearchContent) || empty($blogSearchContent))
{
    $blogSearchContent = ""; // default to nothing for query
}

// Save search criteria - used during blog display index.php
$search_info = array($blogSearchTitle, $blogSearchContent);
saveSearchInfo($search_info);

header('Location: index.php');


?>

