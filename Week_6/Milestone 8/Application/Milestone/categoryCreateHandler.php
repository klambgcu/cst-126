<?php
session_start();

/* 
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-16
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone 6
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone - display table of existing categories
 * 2. Reusable functions
 * ---------------------------------------------------------------
 */

require_once('util_funcs.php');

// store registration parameters
$categoryName = filter_input(INPUT_POST,'CategoryName');

try
{
    // Get Database Connection
    $db = dbConnect();

    // Title must be unique
    $sql = "SELECT * FROM categories WHERE CATEGORY_NAME = :categoryName";
    $statement = $db->prepare($sql);
    $statement->bindValue(':categoryName', $categoryName);
    $statement->execute();
    $row = $statement->fetchAll(\PDO::FETCH_ASSOC);
    $num_rows = count($row);
    
    $statement->closeCursor();
    $statement = null;
    
    if ($num_rows == 0)
    {
        // Define SQL prepare statement and bind values
        $sql = "INSERT INTO categories (CATEGORY_NAME) " .
               "VALUES (:categoryName)";
        
        $statement1 = $db->prepare($sql);
        $statement1->bindValue(':categoryName',   $categoryName);
   
        // Execute insert query
        $statement1->execute();
    }
    else 
    {
        $db = null;
        $_SESSION['errMsg'] = "Category Name Must Be Unique.";
        header('Location: admin_create_category.php');
        exit();
    }
} catch (PDOException $e)
{
    $error_message = $e->getMessage();
    include('database_error.php');
    exit();
}

// Close statement and connection
$statement1->closeCursor();
$statement1 = null;
$db = null;

header('Location: index.php');

?>