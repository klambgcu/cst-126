<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-12
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Activity 4
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 4
 * 2. Reusable functions
 * ---------------------------------------------------------------
 */

require_once('myfuncs.php');

function getAllUsers()
{
    // Get Database Connection
    $db = dbConnect();
    
    // Define SQL prepare statement and bind values
    $sql = "SELECT ID, FIRST_NAME, LAST_NAME FROM users";
    $statement1 = $db->prepare($sql);
    
    // Execute select query
    $statement1->execute();   
    
    // return records as associative array - could use fetchAll
    $statement1->setFetchMode(PDO::FETCH_ASSOC);
    
    // add records to users array
    $users = array();
    $index = 0;
    while ($row = $statement1->fetch())
    {
        $users[$index] = array($row["ID"], $row["FIRST_NAME"], $row["LAST_NAME"]);
        ++$index;
    }
    
    // Close statement and connection
    $statement1->closeCursor();
    $statement1 = null;
    $db = null;
    
    return $users;
}

function getUsersByFirstName($searchPattern)
{   
    // Get Database Connection
    $db = dbConnect();
    
    // Define SQL prepare statement and bind values
    $sql = "SELECT ID, FIRST_NAME, LAST_NAME FROM users WHERE FIRST_NAME LIKE :searchPattern";
    $statement1 = $db->prepare($sql);
    $statement1->bindValue(':searchPattern', $searchPattern);

    // Execute select query
    $statement1->execute();

    // return records as associative array - could use fetchAll
    $statement1->setFetchMode(PDO::FETCH_ASSOC);
    
    // add records to users array
    $users = array();
    $index = 0;
    while ($row = $statement1->fetch())
    {
        $users[$index] = array($row["ID"], $row["FIRST_NAME"], $row["LAST_NAME"]);
        ++$index;
    }
    
    // Close statement and connection
    $statement1->closeCursor();
    $statement1 = null;
    $db = null;
 
   // Return null if zero records found
    if (count($users) == 0)
        return null;
    else
        return $users;
}
?>
