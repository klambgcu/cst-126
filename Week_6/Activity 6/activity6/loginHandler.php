<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-04-26
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone (Blog Site)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 3
 * 2. Obtain form data
 * 3. Handle Login Validation
 * 4. Transistion from procedural connection to PDO object
 * ---------------------------------------------------------------
 */

require_once('myfuncs.php');


// store registration parameters
$username = filter_input(INPUT_POST,'UserName');
$password = filter_input(INPUT_POST,'Password');

// Validate user entry
$valid_input = true;

// Validate user name
// Note: Applying required on the html field(s) makes this unnecessary
if (is_null($username) || empty($username)) {
    $valid_input = false;
    echo "The User Name field is a required field and cannot be blank.<br />";
}

// Validate password
if (is_null($password) || empty($password)) {
    $valid_input = false;
    echo "The Password field is a required field and cannot be blank.<br />";
}

// Check and continue only if input fields are valid
if ($valid_input) {
    
    // Get Database Connection
    $db = dbConnect();
    
    // Define SQL prepare statement and bind values
    $sql = "SELECT * FROM users WHERE USERNAME = :user AND PASSWORD = :pass";
    $statement1 = $db->prepare($sql);
    $statement1->bindValue(':user', $username);
    $statement1->bindValue(':pass', $password);
    
    // Execute insert query
    $statement1->execute();
    $row = $statement1->fetchAll(\PDO::FETCH_ASSOC);
    $num_rows = count($row);
    
    // Determine if login successful
    if ($num_rows === 1)
    {
        // Save User ID in Session
        saveUserId($row[0]["ID"]);
        include('loginResponse.php ');
    }
    elseif ($num_rows === 0) 
    {
        $message = "Login failed.";
        include('loginFailed.php ');
    }
    elseif ($num_rows >= 2)
    {
        $message = "There are multiple users have registered.";
        include('loginFailed.php ');
    }
    else
    {
        $message = "SQL ERROR NO: " . $db->errorCode() . " ERR MSG: ". $db->errorInfo();
        include('loginFailed.php');
    }
    
    // Close statement and connection
    $statement1->closeCursor();
    $statement1 = null;
    $db = null;
}

?>
