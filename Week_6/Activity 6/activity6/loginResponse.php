<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-04-26
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Activity 4
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 4
 * 2. Obtain form data
 * 3. Handle Login Validation
 * 4. Transistion from procedural connection to PDO object
 * 5. Added Reusable code for retrieving and display all users
 * ---------------------------------------------------------------
 */

require_once('utility.php');

 ?>

<!DOCTYPE html>
<html>
	<!--  the head section -->
	<head>
		<title>Login Response</title>
	</head>

	<!-- the body section -->
	<body>
	<main>
		<h1>Login Response</h1>
		<h2>Login was successful: <?php echo " " . $username ?></h2>
		<br />
		<a href="whoAmI.php">Who Am I</a>
		<br />
		<br />
		
<?php
 	$users = getAllUsers();
    include('_displayUsers.php'); 
 ?>

	</main>
	</body>

</html>