<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-17
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Activity 6.2
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 6.2
 * 2. Object Oriented Programming - SuperHero
 * 3. Base class for super hero game
 * ---------------------------------------------------------------
 */

class SuperHero
{
    private $name;
    private $health;
    private $isDead;
    
    function __construct($name, $initialHealth)
    {
        $this->name = $name;
        $this->health = $initialHealth;
        $this->isDead = false;
    }
    
    /**
     * @param SuperHero $opponent
     */
    function Attack($opponent)
    {
        $damage = rand(1,10);
        $opponent->DetermineHealth($damage);
        echo $this->getName() . " attacks " . $opponent->getName() . " inflicting $damage points. ";
        echo $opponent->getName() . " has " . $opponent->getHealth() . " remaining health points.<br />";
    }
    
    /**
     * @param integer $damage
     */
    function DetermineHealth($damage)
    {
        $this->health -= $damage;
        if ($this->health <= 0) $this->isDead = true;
    }
    
    /**
     * @return boolean $isDead
     */
     function isDead()
    {
        return $this->isDead;
    }
    
    /**
     * @return String $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return integer $health
     */
    public function getHealth()
    {
        return $this->health;
    }

    /**
     * @return boolean $dead
     */
    public function getIsDead()
    {
        return $this->isDead;
    }

    /**
     * @param String $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param integer $health
     */
    public function setHealth($health)
    {
        $this->health = $health;
    }

    /**
     * @param boolean $isDead
     */
    public function setIsDead($isDead)
    {
        $this->isDead = $isDead;
    }
}

