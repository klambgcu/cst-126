<?php
// Define database connection parameters
$servername = "localhost";
$username = "root";
$password = "root";
$database = "activity1";

// store registration parameters
$firstname = $_POST["FirstName"];
$lastname = $_POST["LastName"];

// Create the connection
$conn = new mysqli($servername, $username, $password, $database);

// Check connection status
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// prepare and bind variables (avoid SQL Injection)
$stmt = $conn->prepare("INSERT INTO users (FIRST_NAME, LAST_NAME) VALUES (?, ?)");
$stmt->bind_param("ss", $firstname, $lastname);

// Execute insert query
$stmt->execute();

echo "New records created successfully";

$stmt->close();
$conn->close();
?>
