<?php

/*
* ---------------------------------------------------------------
* Name      : Kelly E. Lamb
* Date      : 2021-04-12
* Class     : CST-126 Database Application Programming I
* Professor : Kondo Litchmore PhD.
* Assignment: Milestone (Blog Site)
* Disclaimer: This is my own work
* ---------------------------------------------------------------
* Description:
* 1. Registration Handler (registerHandler.php)
* 2. Retrieves fields from register.html
* 3. Stores in database
* ---------------------------------------------------------------
*/

// Define database connection parameters
$db_servername = "localhost";
$db_username = "root";
$db_password = "root";
$db_database = "cst-126";

// store registration parameters
$firstname = $_POST["FirstName"];
$lastname = $_POST["LastName"];
$email = $_POST["Email"];
$password = $_POST["Password"];
$mobile = $_POST["Mobile"];
$birthdate = $_POST["Birthdate"];
$gender = $_POST["Gender"];

// Convert birthdate to string for insert "YYYY-MM-DD" format
$bdate = new DateTime($birthdate);
$bdate_str = $bdate->format("Y-m-d");

// Create the connection
$conn = new mysqli($db_servername, $db_username, $db_password, $db_database);

// Check connection status
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// prepare and bind variables (avoid SQL Injection)
$stmt = $conn->prepare("INSERT INTO users (FIRST_NAME, LAST_NAME, EMAIL, MOBILE, PASSWORD, BIRTHDATE, GENDER) VALUES (?, ?, ?, ?, ?, ?, ?)");
$stmt->bind_param("ssssssi", $firstname, $lastname, $email, $mobile, $password, $bdate_str, $gender);

// Execute insert query
$stmt->execute();

echo "New records created successfully";

$stmt->close();
$conn->close();








?>
