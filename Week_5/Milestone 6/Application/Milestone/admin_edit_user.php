<?php
session_start();
require_once 'util_funcs.php';
?>

<!DOCTYPE html>

<!-- 
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-16
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone (Blog Site)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Edit Posts (admin_edit_user.php)
 * 2. Simple place holder
 * 3. TO DO: Add card/image/detail
 * ---------------------------------------------------------------
 -->
 
<html>
<head>
<meta charset="ISO-8859-1">
<link rel=stylesheet href="css/main_nav.css" />
<link rel=stylesheet href="css/post_entries.css" />
<title>Amazing Blog Site - Edit User</title>
</head>
<body>

<?php require_once '_main_menu.php';?>

	<div align="center">
    	<hr><br />
    	<h1>Amazing Blog Site!</h1>
    	<hr><br />
    	<h1>Edit User Listing</h1>
	</div>

<br />

<?php
 	$users = getAllUsers();
    include('_displayUsers.php'); 
?>


</body>
</html>