<?php session_start(); ?>
<!DOCTYPE html>

<!-- 
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-02
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone (Blog Site)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Reports (blogCreate.php)
 * 2. Form to enter blog content
 * 3. 
 * ---------------------------------------------------------------
 -->
 
<html>
<head>
<meta charset="ISO-8859-1">
<link rel=stylesheet href="css/main_nav.css" />
<title>Amazing Blog Site - Create</title>
</head>
<body>

<?php require_once '_main_menu.php';?>

	<div align="center">
    	<hr><br />
    	<h1>Amazing Blog Site!</h1>
    	<hr><br />
    	<div align="center">
    		<form action="blogCreateHandler.php" method="POST">
    		    <h1>Blog Create</h1>
    		    <p>Please fill in this form to create a blog entry.</p>
    		    <hr><br />
    		    <div id="error_message">
					<?php if(!empty($_SESSION['errMsg'])) { echo $_SESSION['errMsg'] . "<br />"; unset($_SESSION['errMsg']); } ?>
    			</div>
    		    <label for="BlogTitle"><b>Title:</b></label>
    		    <input type="text" placeholder="Enter a Blog Title" name="BlogTitle" id="BlogTitle" required><br /><br />
    				
    		    <label for="BlogContent"><b>Blog Content:</b></label>
    		    <textarea name="BlogContent" id = "BlogContent" rows="5" cols="100" maxlength="500" required></textarea>
    			<br /><br />
    		    <button type="submit">Submit</button><br /><br />
    		    <hr>
    		</form>
		</div>
	</div>

</body>
</html>