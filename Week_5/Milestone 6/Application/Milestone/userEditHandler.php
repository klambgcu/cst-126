<?php

/*
* ---------------------------------------------------------------
* Name      : Kelly E. Lamb
* Date      : 2021-05-16
* Class     : CST-126 Database Application Programming I
* Professor : Kondo Litchmore PhD.
* Assignment: Milestone (Blog Site)
* Disclaimer: This is my own work
* ---------------------------------------------------------------
* Description:
* 1. Edit User Handler (userEditHandler.php)
* 2. Retrieves fields from _editUser.php
* 3. Stores in database
* ---------------------------------------------------------------
*/

require_once('util_funcs.php');

// store registration parameters
$firstname = filter_input(INPUT_POST,'FirstName');
$lastname  = filter_input(INPUT_POST,'LastName');
$email     = filter_input(INPUT_POST,'Email');
$password  = filter_input(INPUT_POST,'Password');
$mobile    = filter_input(INPUT_POST,'Mobile');
$birthdate = filter_input(INPUT_POST,'Birthdate');
$gender    = filter_input(INPUT_POST,'Gender');
$role_id   = filter_input(INPUT_POST,'UserRoleID');
$user_id   = filter_input(INPUT_POST,'UserID');

// Convert birthdate to string for insert "YYYY-MM-DD" format
$bdate = new DateTime($birthdate);
$bdate_str = $bdate->format("Y-m-d");

try
{
    // Get Database Connection
    $db = dbConnect();
    
    // Define SQL prepare statement and bind values
    $sql = "UPDATE users " . 
           "   SET FIRST_NAME = :firstname, LAST_NAME = :lastname, EMAIL = :email, ". 
           "       MOBILE = :mobile, PASSWORD = :password, BIRTHDATE = :bdate_str, " . 
           "       GENDER = :gender, ROLE_ID = :role_id " .
           " WHERE ID = :user_id "; 
    $statement1 = $db->prepare($sql);
    $statement1->bindValue(':firstname', $firstname);
    $statement1->bindValue(':lastname',  $lastname);
    $statement1->bindValue(':email',     $email);
    $statement1->bindValue(':mobile',    $mobile);
    $statement1->bindValue(':password',  $password);
    $statement1->bindValue(':bdate_str', $bdate_str);
    $statement1->bindValue(':gender',    $gender);
    $statement1->bindValue(':role_id',   $role_id);
    $statement1->bindValue(':user_id',   $user_id);
    
    // Execute insert query
    $statement1->execute();
} catch(PDOException $e)
{
    $error_message = $e->getMessage();
    include('database_error.php');
    exit();
}

// Close statement and connection
$statement1->closeCursor();
$statement1 = null;
$db = null;

header('Location: admin_edit_user.php');

?>
