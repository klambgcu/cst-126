<!DOCTYPE html>

<!-- 
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-02
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone (Blog Site)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Reports (blogReports.php)
 * 2. Simple place holder
 * 3. TO DO: Add actual searching/reports
 * ---------------------------------------------------------------
 -->
 
<html>
<head>
<meta charset="ISO-8859-1">
<link rel=stylesheet href="css/main_nav.css" />
<title>Amazing Blog Site - Reports</title>
</head>
<body>

<?php require_once '_main_menu.php';?>

	<div align="center">
    	<hr><br />
    	<h1>Amazing Blog Site!</h1>
    	<hr><br />
    	<h1>Reports</h1>
    	<h3>Report 1</h3>
    	<h3>Place Holder</h3>
    	<h3>Search / Report Details Go Here.</h3>
	</div>

</body>
</html>