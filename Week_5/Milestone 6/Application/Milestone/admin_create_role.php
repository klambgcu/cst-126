<?php
session_start();
require_once 'util_funcs.php';
?>

<!DOCTYPE html>

<!-- 
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-16
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone (Blog Site)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create New Category (admin_create_role.php)
 *
 * ---------------------------------------------------------------
 -->
 
<html>
<head>
<meta charset="ISO-8859-1">
<link rel=stylesheet href="css/main_nav.css" />
<link rel=stylesheet href="css/post_entries.css" />
<title>Amazing Blog Site - New Role</title>
</head>
<body>

<?php require_once '_main_menu.php'; ?>

	<div align="center">
    	<hr><br />
    	<h1>Amazing Blog Site!</h1>
    	<hr><br />
    	<h1>New Role</h1><br />
	</div>

	<div align="center">
		<form action="roleCreateHandler.php" method="POST">
		    <hr><br />

		    <div id="error_message">
				<?php if(!empty($_SESSION['errMsg'])) { echo $_SESSION['errMsg'] . "<br />"; unset($_SESSION['errMsg']); } ?>
			</div>
		
		    <label for="RoleName"><b>Role Name:</b></label>
		    <input type="text" placeholder="Enter role name" name="RoleName" id="RoleName" maxlength="50" required><br /><br />
									
		    <label for="RoleDescription"><b>Role Description:</b></label>
		    <input type="text" placeholder="Enter role description" name="RoleDescription" id="RoleDescription" maxlength="100" required><br /><br />


		    <button type="submit">Create Role</button><br /><br />
		    <hr>
		</form>
	</div>
<br />

<?php
 	$roles = getAllRoles();
    include('_displayRoles.php'); 
?>

</body>
</html>
