<?php require_once 'util_funcs.php' ?>

<!-- 
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-16
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone 6
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone 4
 * 2. Reusable functions
 * ---------------------------------------------------------------
 -->

<table id="post_entries">
    <tr>
        <th>ID</th>
        <th>Category</th>
        <th>Title</th>
        <th>Blog Content</th>
        <th>Contributor</th>
        <th>Date</th>       
        <th>Action</th>       
    <tr>

<?php

    $user_info = getUserInfo();
    $role_id = $user_info[0]["ROLE_ID"];
    $user_id = getUserId();
    
    for($x=0;$x < count($blogs); $x++)
    {
        echo "  <tr>\n";
        echo "      <td>" . $blogs[$x][0] . "</td>\n";
        echo "      <td>" . $blogs[$x][9] . "</td>\n";
        echo "      <td>" . $blogs[$x][1] . "</td>\n";
        echo "      <td>" . $blogs[$x][3] . "</td>\n";
        echo "      <td>" . $blogs[$x][10] . " " . $blogs[$x][11] . "</td>\n";
                    $date=date_create($blogs[$x][4]);
        echo "      <td>" . date_format($date,"Y-m-d") . "</td>\n";
        
        // Add edit/delete actions per roles / user id
        echo "      <td>";
        
        if ($role_id == 3) // Admin - Edit/Delete
        {
            echo "<a href=blogChangeHandler.php?id=" . $blogs[$x][0] . "&mode=0>Edit<a>";
            echo "&nbsp;&nbsp;|&nbsp;&nbsp;";
            echo "<a href=blogChangeHandler.php?id=" . $blogs[$x][0] . "&mode=1>Delete<a>";
            echo "</td>\n";
            
        }
        else if (($user_id == $blogs[$x][5]) || ($role_id == 2) ) // edit own blogs or advanced user
        {
            echo "<a href=blogChangeHandler.php?id=" . $blogs[$x][0] . "&mode=0>Edit<a>";
            echo "</td>\n";
        }
        else
        {
            echo "&nbsp;</td>\n";
        }
        
        
        echo "  </tr>\n";
	}
 ?>

</table>
