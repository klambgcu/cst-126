<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-04-19
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone (Blog Site)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 2
 * 2. Retrieve all users from database
 * 3. Present them to user in response
 * ---------------------------------------------------------------
 */

// Define database connection parameters
$db_servername = "localhost";
$db_username = "root";
$db_password = "root";
$db_database = "activity1";

// Create the connection
$conn = new mysqli($db_servername, $db_username, $db_password, $db_database);

// Check connection status
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM users";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "id: " . $row["ID"]. " First Name: " . $row["FIRST_NAME"]. " Last Name: " . $row["LAST_NAME"]. "<br>";
    }
} else {
    echo "0 results";
}

// Close the connection
$conn->close();

?>
