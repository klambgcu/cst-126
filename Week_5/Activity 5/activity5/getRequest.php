<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-16
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Activity 5.2
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 5.2
 * 2. Obtain form data
 * ---------------------------------------------------------------
 */

$id = filter_input(INPUT_GET, 'id');
$mode = filter_input(INPUT_GET, 'mode');

echo "<h1>Activity 5.2 Response</h1>";
echo "<hr />";
echo "ID = " . $id . "<br />";
echo "MODE = " . $mode . "<br />";
echo "<hr />";
?>
