<?php

/*
* ---------------------------------------------------------------
* Name      : Kelly E. Lamb
* Date      : 2021-04-12
* Class     : CST-126 Database Application Programming I
* Professor : Kondo Litchmore PhD.
* Assignment: Milestone (Blog Site)
* Disclaimer: This is my own work
* ---------------------------------------------------------------
* Description:
* 1. Registration Handler (registerHandler.php)
* 2. Retrieves fields from register.html
* 3. Stores in database
* TO DO:
* 1. Validate that email does not already exist in the database
* ---------------------------------------------------------------
*/

require_once('util_funcs.php');

// store registration parameters
$firstname = filter_input(INPUT_POST,'FirstName');
$lastname  = filter_input(INPUT_POST,'LastName');
$email     = filter_input(INPUT_POST,'Email');
$password  = filter_input(INPUT_POST,'Password');
$mobile    = filter_input(INPUT_POST,'Mobile');
$birthdate = filter_input(INPUT_POST,'Birthdate');
$gender    = filter_input(INPUT_POST,'Gender');
$role_id   = 1; // Default to Basic - Administrator can decide to change

// Convert birthdate to string for insert "YYYY-MM-DD" format
$bdate = new DateTime($birthdate);
$bdate_str = $bdate->format("Y-m-d");

try
{
    // Get Database Connection
    $db = dbConnect();
    
    // Define SQL prepare statement and bind values
    $sql = "INSERT INTO users (FIRST_NAME, LAST_NAME, EMAIL, MOBILE, PASSWORD, BIRTHDATE, GENDER, ROLE_ID) " .
           "VALUES (:firstname, :lastname, :email, :mobile, :password, :bdate_str, :gender, :role_id)";
    $statement1 = $db->prepare($sql);
    $statement1->bindValue(':firstname', $firstname);
    $statement1->bindValue(':lastname',  $lastname);
    $statement1->bindValue(':email',     $email);
    $statement1->bindValue(':mobile',    $mobile);
    $statement1->bindValue(':password',  $password);
    $statement1->bindValue(':bdate_str', $bdate_str);
    $statement1->bindValue(':gender',    $gender);
    $statement1->bindValue(':role_id',   $role_id);
    
    // Execute insert query
    $statement1->execute();
} catch(PDOException $e)
{
    $error_message = $e->getMessage();
    include('database_error.php');
    exit();
}

// Close statement and connection
$statement1->closeCursor();
$statement1 = null;
$db = null;

header('Location: index.php');

?>
