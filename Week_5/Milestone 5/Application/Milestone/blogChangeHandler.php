<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-16
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone (Blog Site)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone - Handle Post Edit / Delete Requests
 * 2. Obtain form data
 * 3. Handle Login Validation
 * ---------------------------------------------------------------
 */

require_once('util_funcs.php');

// store registration parameters
$blog_id = filter_input(INPUT_GET,'id');
$mode    = filter_input(INPUT_GET,'mode'); // 0 - Edit, 1 - Delete

// Validate mode operations
if ( ($mode < 0) || ($mode > 1) )
{
    echo "Invalid Request Operation - Contact Administrator.<br />";
    exit();
}

try
{
    // Get Database Connection
    $db = dbConnect();
    
    // Define SQL prepare statement and bind values
    $sql = "SELECT p.*, u.FIRST_NAME, u.LAST_NAME, c.CATEGORY_NAME FROM posts p, users u, categories c WHERE p.ID = :blog_id AND p.POSTED_BY = u.ID AND p.CATEGORY_ID = c.ID";
    $statement1 = $db->prepare($sql);
    $statement1->bindValue(':blog_id', $blog_id);
    
    // Execute query
    $statement1->execute();
    $blog_row = $statement1->fetchAll(\PDO::FETCH_ASSOC);
    
    // Define SQL prepare statement and bind values
    $sql = "SELECT * FROM categories ORDER BY ID";
    $statement1 = $db->prepare($sql);
    
    // Execute query
    $statement1->execute();
    $categories = $statement1->fetchAll(\PDO::FETCH_ASSOC);

} catch(PDOException $e)
{
    $error_message = $e->getMessage();
    include('database_error.php');
        exit();
}

// Close statement and connection
$statement1->closeCursor();
$statement1 = null;
$db = null;

if ($mode == 0)
{
    include('_editBlog.php');
}
else
{
    include('_deleteBlog.php'); 
}
?>