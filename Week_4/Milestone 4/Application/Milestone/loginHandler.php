<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-04-19
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone (Blog Site)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone - Handle Login Entry
 * 2. Obtain form data
 * 3. Handle Login Validation
 * ---------------------------------------------------------------
 */

require_once('util_funcs.php');

// store registration parameters
$email    = filter_input(INPUT_POST,'Email');
$password = filter_input(INPUT_POST,'Password');

// Validate user entry
$valid_input = true;

// Validate user email
// Note: Applying required on the html field(s) makes this unnecessary
if (is_null($email) || empty($email)) {
    $valid_input = false;
    echo "The Email field is a required field and cannot be blank.<br />";
}

// Validate password
if (is_null($password) || empty($password)) {
    $valid_input = false;
    echo "The Password field is a required field and cannot be blank.<br />";
}

// Check and continue only if input fields are valid
if ($valid_input)
{
    try
    {
        // Get Database Connection
        $db = dbConnect();
        
        // Define SQL prepare statement and bind values
        $sql = "SELECT * FROM users WHERE EMAIL = :email AND PASSWORD = :pass";
        $statement1 = $db->prepare($sql);
        $statement1->bindValue(':email', $email);
        $statement1->bindValue(':pass',  $password);
        
        // Execute query
        $statement1->execute();
        $row = $statement1->fetchAll(\PDO::FETCH_ASSOC);
        $num_rows = count($row);
        
        // Determine if login successful
        if ($num_rows === 1)
        {
            // Save User ID in Session
            saveUserId($row[0]["ID"]);
            saveUserRoleId($row[0]["ROLE_ID"]);
            saveUserInfo($row);
            print_r(getUserInfo());
            header('Location: index.php'); // Redirect to home page on success
        }
        elseif ($num_rows === 0)
        {
            $message = "Login failed.";
            include('loginFailed.php ');
        }
        elseif ($num_rows >= 2)
        {
            $message = "There are multiple users have registered.";
            include('loginFailed.php ');
        }
        else
        {
            $error_message = $e->getMessage();
            include('database_error.php');
            exit();
        }
    } catch(PDOException $e)
    {
        $error_message = $e->getMessage();
        include('database_error.php');
        exit();
    }

    // Close statement and connection
    $statement1->closeCursor();
    $statement1 = null;
    $db = null;
}

?>
