<!-- 
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-12
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone 4
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone 4
 * 2. Reusable functions
 * ---------------------------------------------------------------
 -->

<table id="post_entries">
    <tr>
        <th>ID</th>
        <th>Category</th>
        <th>Title</th>
        <th>Blog Content</th>
        <th>Contributor</th>
        <th>Date</th>       
    </tr>

<?php
    for($x=0;$x < count($blogs); $x++)
    {
        echo "  <tr>\n";
        echo "      <td>" . $blogs[$x][0] . "</td>\n";
        echo "      <td>" . $blogs[$x][2] . "</td>\n";
        echo "      <td>" . $blogs[$x][1] . "</td>\n";
        echo "      <td>" . $blogs[$x][3] . "</td>\n";
        echo "      <td>" . $blogs[$x][5] . "</td>\n";
        echo "      <td>" . $blogs[$x][4] . "</td>\n";
        echo "  </tr>\n";
	}
 ?>

</table>
