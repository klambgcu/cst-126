<!-- 
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-12
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Activity 4
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 4
 * 2. Reusable functions
 * ---------------------------------------------------------------
 -->

<table>
    <tr>
        <th>ID</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Role</th>
    </tr>

<?php
    for($x=0;$x < count($users); $x++)
    {
        echo "  <tr>\n";
        echo "      <td>" . $users[$x][0] . "</td>\n";
        echo "      <td>" . $users[$x][1] . "</td>\n";
        echo "      <td>" . $users[$x][2] . "</td>\n";
        echo "      <td>" . $users[$x][3] . "</td>\n";
        echo "  </tr>\n";
	}
 ?>

</table>
