
<!-- 
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-04-19
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone (Blog Site)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Main Menu (_main_menu.php)
 * 2. 
 * 3. 
 * ---------------------------------------------------------------
 -->

<nav id="main_nav">
	<ul>
		<li><a href="index.html">Home</a></li>
		<li>
			<a href="">Accounts</a>
			<ul>
				<li><a href="register.php">Registration...</a></li>
				<li><a href="login.php">Login...</a></li>
			</ul>
		</li>
	</ul>
</nav>
