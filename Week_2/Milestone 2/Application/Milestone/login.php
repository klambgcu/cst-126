<!DOCTYPE html>

<!-- 
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-04-19
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone (Blog Site)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Login Form (login.html)
 * 2. Calls loginHandler.php
 * 3. Added Main Menu requirement
 * ---------------------------------------------------------------
 -->

<html>
<head>
<meta charset="ISO-8859-1">
<link rel=stylesheet href="css/main_nav.css" />
<title>Login Form</title>
</head>
<body>

<?php require_once '_main_menu.php';?>

	<div align="center">
		<form action="loginHandler.php" method="POST">
		    <h1>Login Form</h1>
		    <p>Please fill in this form to login to the application.</p>
		    <hr><br />
		
		    <label for="Email"><b>Email:</b></label>
		    <input type="email" placeholder="Enter email address" name="Email" id="Email" required><br /><br />
				
		    <label for="Password"><b>Password:</b></label>
		    <input type="password" placeholder="Password Length 8 minimum" name="Password" id="Password" pattern=".{8,}" required><br /><br />
					
		    <button type="submit">Login</button><br /><br />
		    <hr>
		</form>
	</div>
	
</body>
</html>