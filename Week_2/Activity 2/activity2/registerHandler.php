<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-04-19
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone (Blog Site)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 1 & 2
 * 2. Obtain form data
 * 3. Stores in database
 * ---------------------------------------------------------------
 */

// Define database connection parameters
$db_servername = "localhost";
$db_username = "root";
$db_password = "root";
$db_database = "activity1";

// store registration parameters
$firstname = $_POST["FirstName"];
$lastname = $_POST["LastName"];
$username = $_POST["UserName"];
$password = $_POST["Password"];

// Validate user entry
$valid_input = true;

// Validate first name
// Note: Applying required on the html field(s) makes this unnecessary
if (is_null($firstname) || empty($firstname)) {
    $valid_input = false;
    echo "The First Name field is a required field and cannot be blank.<br />";
}

// Validate last name
if (is_null($lastname) || empty($lastname)) {
    $valid_input = false;
    echo "The Last Name field is a required field and cannot be blank.<br />";
}

// Validate user name
if (is_null($username) || empty($username)) {
    $valid_input = false;
    echo "The User Name field is a required field and cannot be blank.<br />";
}

// Validate password
if (is_null($password) || empty($password)) {
    $valid_input = false;
    echo "The Password field is a required field and cannot be blank.<br />";
}

// Check and continue only if input fields are valid 
if ($valid_input) {

    // Create the connection
    $conn = new mysqli($db_servername, $db_username, $db_password, $db_database);
    
    // Check connection status
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    
    // prepare and bind variables (avoid SQL Injection)
    $stmt = $conn->prepare("INSERT INTO users (FIRST_NAME, LAST_NAME, USERNAME, PASSWORD) VALUES (?, ?, ?, ?)");
    $stmt->bind_param("ssss", $firstname, $lastname, $username, $password);
    
    // Execute insert query
    $stmt->execute();
    
    echo "New records created successfully";
    
    // Close statement and connection
    $stmt->close();
    $conn->close();
}

?>
