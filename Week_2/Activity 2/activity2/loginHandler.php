<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-04-19
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone (Blog Site)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Activity 2
 * 2. Obtain form data
 * 3. Handle Login Validation
 * ---------------------------------------------------------------
 */

// Define database connection parameters
$db_servername = "localhost";
$db_username = "root";
$db_password = "root";
$db_database = "activity1";

// store registration parameters
$username = $_POST["UserName"];
$password = $_POST["Password"];

// Validate user entry
$valid_input = true;

// Validate user name
// Note: Applying required on the html field(s) makes this unnecessary
if (is_null($username) || empty($username)) {
    $valid_input = false;
    echo "The User Name field is a required field and cannot be blank.<br />";
}

// Validate password
if (is_null($password) || empty($password)) {
    $valid_input = false;
    echo "The Password field is a required field and cannot be blank.<br />";
}

// Check and continue only if input fields are valid
if ($valid_input) {
    
    // Create the connection
    $conn = new mysqli($db_servername, $db_username, $db_password, $db_database);
    
    // Check connection status
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    
    // prepare and bind variables (avoid SQL Injection)
    $stmt = $conn->prepare("SELECT * FROM users WHERE USERNAME = ? AND PASSWORD = ?");
    $stmt->bind_param("ss", $username, $password);
    
    // Execute insert query
    $stmt->execute();
    
    $result = $stmt->get_result();
    if($result->num_rows === 1) {
        echo "Login was successful. <br />";
    } elseif ($result->num_rows === 0) {
        echo "Login failed. <br />";
    } elseif ($result->num_rows >= 2) {
        echo "There are multiple users have registered. <br />";
    } else {
        echo "SQL ERROR NO: " . $conn->errno . " ERR MSG: ". $conn->error . "<br />"; 
    }
    
    // Close statement and connection
    $stmt->close();
    $conn->close();
}

?>
