<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-04-26
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Activity 3
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. myfuncs.php - a collection of functions
 * 2. Convert connection from procedural to PDO Object
 * 3. Utilizes database_error.php to display connection error(s). 
 * 4.
 * ---------------------------------------------------------------
 */

//
// Create a database PDO connection
// Returns the connection
// Throw exception to database_error display form
//
function dbConnect() {
        
    // Define database connection parameters
    $connect_string = 'mysql:host=localhost;dbname=activity1';
    $db_username = "root";
    $db_password = "root";
    
    try
    {
        // Create a PDO object
        $db_connection = new PDO($connect_string, $db_username, $db_password);
    } catch (PDOException $e)
    {
        $error_message = $e->getMessage();
        include('database_error.php');
        exit();
    }
    
    return $db_connection;
}

function saveUserId($id)
{
    session_start();
    $_SESSION["USER_ID"] = $id;
}
function getUserId()
{
    session_start();
    return $_SESSION["USER_ID"];
}


?>