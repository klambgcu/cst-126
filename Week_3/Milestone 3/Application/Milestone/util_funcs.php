<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-02
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. util_funcs.php - a collection of functions
 * 2.
 * 3.
 * 4.
 * ---------------------------------------------------------------
 */


function saveUserId($id)
{
    session_start();
    $_SESSION["USER_ID"] = $id;
}

function getUserId()
{
    session_start();
    return $_SESSION["USER_ID"];
}

function saveUserFirstName($firstName)
{
    session_start();
    $_SESSION["USER_FIRSTNAME"] = $firstName;
}

function getUserFirstName()
{
    session_start();
    return $_SESSION["USER_FIRSTNAME"];
}

function saveUserLasttName($lastName)
{
    session_start();
    $_SESSION["USER_LASTNAME"] = $lastName;
}

function getUserLastName()
{
    session_start();
    return $_SESSION["USER_LASTNAME"];
}

function saveUserInfo($user_info_array)
{
    session_start();
    $_SESSION["USER_INFO"] = $user_info_array;
}

function getUserInfo()
{
    session_start();
    return $_SESSION["USER_INFO"];
}


?>