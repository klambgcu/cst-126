<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-04-19
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone (Blog Site)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Milestone - Handle Login Entry
 * 2. Obtain form data
 * 3. Handle Login Validation
 * ---------------------------------------------------------------
 */

require_once('util_funcs.php');

// Define database connection parameters
$db_servername = "localhost";
$db_username = "root";
$db_password = "root";
$db_database = "cst-126";

// store registration parameters
$email = $_POST["Email"];
$password = $_POST["Password"];

// Validate user entry
$valid_input = true;

// Validate user email
// Note: Applying required on the html field(s) makes this unnecessary
if (is_null($email) || empty($email)) {
    $valid_input = false;
    echo "The Email field is a required field and cannot be blank.<br />";
}

// Validate password
if (is_null($password) || empty($password)) {
    $valid_input = false;
    echo "The Password field is a required field and cannot be blank.<br />";
}

// Check and continue only if input fields are valid
if ($valid_input) {
    
    // Create the connection
    $conn = new mysqli($db_servername, $db_username, $db_password, $db_database);
    
    // Check connection status
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    
    // prepare and bind variables (avoid SQL Injection)
    $stmt = $conn->prepare("SELECT * FROM users WHERE EMAIL = ? AND PASSWORD = ?");
    $stmt->bind_param("ss", $email, $password);
    
    // Execute insert query
    $stmt->execute();
    
    $result = $stmt->get_result();
    if($result->num_rows === 1)
    {
        // Get user info and store in session
        $user_info = $result -> fetch_assoc();
        saveUserInfo($user_info);
        print_r(getUserInfo());
        header('Location: index.php'); // Redirect to home page on success
    }
    elseif ($result->num_rows === 0)
    {
        echo "Login failed. <br />";
        echo "<br /><br /><a href='index.php'>Welcome Page</a><br />";
    }
    elseif ($result->num_rows >= 2)
    {
        echo "There are multiple users have registered. <br />";
        echo "<br /><br /><a href='index.php'>Welcome Page</a><br />";
    }
    else
    {
        echo "SQL ERROR NO: " . $conn->errno . " ERR MSG: ". $conn->error . "<br />"; 
    }
    echo "<br /><br /><a href='index.php'>Welcome Page</a><br />";

    // Close result set, statement, and connection
    $result->close();
    $stmt->close();
    $conn->close();
}

?>
