<!DOCTYPE html>

<!-- 
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-04-19
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone (Blog Site)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Welcome / Default Page (index.html)
 * 2. Initial Menu Options for home, login, register
 * 3. 
 * ---------------------------------------------------------------
 -->
 
<html>
<head>
<meta charset="ISO-8859-1">
<link rel=stylesheet href="css/main_nav.css" />
<title>Amazing Blog Site</title>
</head>
<body>

<?php require_once '_main_menu.php';?>

	<div align="center">
	<hr><br />
	<h1>Welcome to the Amazing Blog Site!</h1>
	<hr><br />
	</div>

</body>
</html>