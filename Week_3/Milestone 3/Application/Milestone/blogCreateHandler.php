<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-05-02
 * Class     : CST-126 Database Application Programming I
 * Professor : Kondo Litchmore PhD.
 * Assignment: Milestone (Blog Site)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Blog Creation Handler (blogCreateHandler.php)
 * 2. Retrieves fields from blogCreate.php
 * 3. Stores in database

 * ---------------------------------------------------------------
 */

require_once('util_funcs.php');

// Define database connection parameters
$db_servername = "localhost";
$db_username = "root";
$db_password = "root";
$db_database = "cst-126";

// store registration parameters
$blogTitle = $_POST["BlogTitle"];
$blogContent = $_POST["BlogContent"];
$categoryId = 1; // Hard coded for now

// Get user id from session
$user_info = getUserInfo();
$userID = $user_info['ID'];

// Create create/update date for insert - "YYYY-MM-DD" format
$cudate = date("Y-m-d");

// Create the conne$blogContentction
$conn = new mysqli($db_servername, $db_username, $db_password, $db_database);

// Check connection status
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// prepare and bind variables (avoid SQL Injection)
$stmt = $conn->prepare("INSERT INTO posts(TITLE, CATEGORY_ID, POST_CONTENT, POSTED_DATE, POSTED_BY, UPDATED_DATE, UPDATED_BY) VALUES (?, ?, ?, ?, ?, ?, ?)");
$stmt->bind_param("sissisi", $blogTitle, $categoryId, $blogContent, $cudate, $userID, $cudate, $userID);

// Execute insert query
$stmt->execute();

echo "New records created successfully";

$stmt->close();
$conn->close();

?>
